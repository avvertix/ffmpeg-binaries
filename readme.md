# FFMpeg Binaries

Static Windows, Linux and MacOS builds of ffmpeg available as a generic package.

This project publishes, as generic binary package, the builds obtained by:

1. https://github.com/BtbN/FFmpeg-Builds
2. https://evermeet.cx/ffmpeg/
3. https://johnvansickle.com/ffmpeg/


Only 64bit builds are available.



## License

Static builds are licensed under [GPLv3](./License)

